L.Control.SuperiorLayerManager = L.Control.extend({
    options: {
        collapsed: true,
        position: 'topright',
        autoZIndex: true,
        enableTabs: false,
        miscTabsName: "Outros"
    },

    initialize: function (baseLayers, overlays, tabs, options) {
        L.setOptions(this, options);

        this._layers = {};
        this._lastZIndex = 0;
        this._handlingClick = false;

        if (tabs) {
            this._tabs = tabs;
            this.options.enableTabs = true;
        }

        for (var i in baseLayers) {
            this._addLayer(baseLayers[i], i);
        }

        for (i in overlays) {
            if (overlays[i].hasOwnProperty("options")) {
                this._addLayer(overlays[i], i, true, overlays[i].options.tab);
            }
        }
    },

    onAdd: function (map) {

        this._initLayout();
        this._update();


        this._activeBaseLayer = this._findActiveBaseLayer();

        map
            .on('layeradd', this._onLayerChange, this)
            .on('layerremove', this._onLayerChange, this);

        return this._container;
    },

    onRemove: function (map) {
        map
            .off('layeradd', this._onLayerChange, this)
            .off('layerremove', this._onLayerChange, this);
    },

    addBaseLayer: function (layer, name) {
        this._addLayer(layer, name);
        this._update();
        return this;
    },

    addOverlay: function (layer, name) {
        this._addLayer(layer, name, true);
        this._update();
        return this;
    },

    removeLayer: function (layer) {
        var id = L.stamp(layer);
        delete this._layers[id];
        this._update();
        return this;
    },

    _findActiveBaseLayer: function () {
        var layers = this._layers
        for (var layerId in layers) {
            if (this._layers.hasOwnProperty(layerId)) {
                var layer = layers[layerId]
                if (!layer.overlay && this._map.hasLayer(layer.layer)) {
                    return layer;
                }
            }
        }
        throw new Error('Control doesn\'t have any active base layer!')
    },

    _recountLayers: function () {
        var i, input, obj,
            inputs = this._form.getElementsByTagName('input'),
            inputsLen = inputs.length;

        for (i = 0; i < inputsLen; i++) {
            input = inputs[i];
            obj = this._layers[input.layerId];

            if (input.checked && !this._map.hasLayer(obj.layer)) {

                this._activeBaseLayer = obj;
            }
        }
    },

    _initLayout: function () {
        var className = 'switch-control-layers',
            container = this._container = L.DomUtil.create('div', "leaflet-bar " + className);

        //Makes this work on IE10 Touch devices by stopping it from firing a mouseout event when the touch is released
        container.setAttribute('aria-haspopup', true);

        if (!L.Browser.touch) {
            L.DomEvent
                .disableClickPropagation(container)
                .disableScrollPropagation(container);
        } else {
            L.DomEvent.on(container, 'click', L.DomEvent.stopPropagation);
        }

        var form = this._form = L.DomUtil.create('form', className + '-list form-layer-list');

        if (this.options.collapsed) {
            var link = this._layersLink = L.DomUtil.create('a', className + '-toggle', container);
            //link.href = '#';
            link.title = 'Layers';

            var linkClose = this._layersLink = L.DomUtil.create('a', 'glyphicon glyphicon-chevron-right switch-control-layers-toggle-expanded', form);
            linkClose.style = 'float:right';
            //linkClose.href = '#';
            linkClose.title = 'LayersClose';

            if (L.Browser.touch) {
                L.DomEvent
                    .on(link, 'click', L.DomEvent.stop)
                    .on(link, "click", this._activeTab, this)
                    .on(link, 'click', this._expand, this);

                L.DomEvent
                    .on(linkClose, 'click', L.DomEvent.stop)
                    .on(linkClose, "click", this._activeTab, this)
                    .on(linkClose, 'click', this._collapse, this);
            }
            else {
                L.DomEvent
                    .on(link, "click", this._activeTab, this)
                    .on(link, 'click', this._collapse, this);

                L.DomEvent
                    .on(linkClose, "click", this._activeTab, this)
                    .on(linkClose, 'click', this._collapse, this);
            }
        } else {
            this._collapse();
        }

        this._baseLayersList = L.DomUtil.create('div', className + '-base', form);

        if (!this.options.enableTabs) {
            this._separator = L.DomUtil.create('div', className + '-separator', form);
        }

        this._overlaysList = L.DomUtil.create('div', className + '-overlays', form);

        if (this.options.enableTabs) {
            this._tabsOverlayers = L.DomUtil.create('ul', 'nav nav-tabs', form);
            $(this._tabsOverlayers).attr('id', 'tabsOverlayers');

            this._tabsContentOverlayers = L.DomUtil.create('div', 'tab-content', form);
            $(this._tabsContentOverlayers).attr('id', 'tabsContent');

            this._tabsList = {};

            for (var t in this._tabs) {
                this._createTab(t, this._tabs[t]);
            }

            if (this.options.miscTabsName) {
                var obj = {
                    //icon: "../images/miscellaneous.png"
                };

                this._createTab(this.options.miscTabsName, obj);
            }
        }


        container.appendChild(form);
    },

    _addLayer: function (layer, name, overlay, tab) {
        var id = L.stamp(layer);

        this._layers[id] = {
            layer: layer,
            name: name,
            overlay: overlay,
            tab: tab
        };

        if (this.options.autoZIndex && layer.setZIndex) {
            this._lastZIndex++;
            layer.setZIndex(this._lastZIndex);
        }
    },

    _update: function () {
        if (!this._container) {
            return;
        }

        this._baseLayersList.innerHTML = '';
        this._overlaysList.innerHTML = '';

        if (this.options.enableTabs) {
            this._tabsOverlayers.innerHTML = '';
            this._tabsContentOverlayers.innerHTML = '';

            this._tabsList = {};

            for (var t in this._tabs) {
                this._createTab(t, this._tabs[t]);
            }

            if (this.options.miscTabsName) {
                var obj = {
                    //icon: "../images/miscellaneous.png"
                };

                this._createTab(this.options.miscTabsName, obj);
            }
        }

        var baseLayersPresent = false,
            overlaysPresent = false,
            i, obj;

        for (i in this._layers) {
            obj = this._layers[i];
            this._addItem(obj);
            overlaysPresent = overlaysPresent || obj.overlay;
            baseLayersPresent = baseLayersPresent || !obj.overlay;
        }

        if (!this.options.enableTabs) {
            this._separator.style.display = overlaysPresent && baseLayersPresent ? '' : 'none';
        }
    },

    _onLayerChange: function (e) {
        this._recountLayers();
        var obj = this._layers[L.stamp(e.layer)];

        if (!obj) {
            return;
        }

        if (!this._handlingClick) {
            //this._update();
        }

        var type = obj.overlay ?
            (e.type === 'layeradd' ? 'overlayadd' : 'overlayremove') :
            (e.type === 'layeradd' ? 'baselayerchange' : null);

        if (type) {
            this._map.fire(type, obj);
        }
    },

    // IE7 bugs out if you create a radio dynamically, so you have to do it this hacky way (see http://bit.ly/PqYLBe)
    _createRadioElement: function (name, checked) {

        var radioHtml = '<input type="radio" class="' + name + '" name="baseLayerElement"';
        if (checked) {
            radioHtml += ' checked="checked"';
        }
        radioHtml += '/>';

        var radioFragment = document.createElement('div');
        radioFragment.innerHTML = radioHtml;

        return radioFragment.firstChild;
    },

    _createSliderElement: function (ctx, lId) {
        var slider = document.createElement('div');
        slider.className = 'switch-control-layers-slider';
        slider.innerHTML += '<br/>'
        slider.id = lId;
        $(slider).slider(
            {
                disabled: true,
                value: 1,
                min: 0,
                max: 1,
                step: 0.01,
                slide: function (event, ui) {
                    this._handlingClick = true;
                    var layer = ctx._layers[this.layerId].layer;
                    layer.setOpacity(ui.value);
                    this._handlingClick = false;
                }
            });


        return slider;
    },

    _createTab: function (tabName, tabIcon) {
        var newTab = L.DomUtil.create('li', '', this._tabsOverlayers)
        var newTabContent = L.DomUtil.create('div', 'tab-pane', this._tabsContentOverlayers);
        $(newTabContent).attr('id', tabName);

        this._tabsList[tabName] = newTabContent;
        var newTabName = L.DomUtil.create('a', '', newTab);

        $(newTabName).attr('id', "tabLink" + tabName);
        $(newTabName).attr('data-target', "#" + tabName);
        $(newTabName).attr('data-toggle', 'tab');

        if (tabIcon.name)
            newTabName.innerHTML = tabIcon.name;
        else if (tabIcon.icon)
            newTabName.innerHTML = '<img src=" ' + tabIcon.icon + '" width="22px" height="22px">';
        else
            newTabName.innerHTML = '<span class="glyphicon glyphicon-home" width="22px" height="22px">';


        if (tabIcon.selected) {
            this._selectedTab = newTabName;
            this._selectedTabContent = newTabContent;
        }

        L.DomEvent.on(newTabName, 'click', (function () {
            this._selectedTab = newTabName;
            return this._selectedTabContent = newTabContent;
        }), this);

        if (L.Browser.touch) {
            L.DomEvent.on(newTabName, "click", L.DomEvent.stop).on(newTabName, "click", this._activeTab, this).on(newTabName, "click", this._expand, this);
        }
    },

    _activeTab: function () {
        return $(this._selectedTab).trigger('click');
    },

    _addItem: function (obj) {
        var label, input, container, control, controlgroup, checked;

        if (obj.overlay) {
            if (this.options.enableTabs) {
                if (obj.tab) {
                    container = this._tabsList[obj.tab];
                } else if (this.options.miscTabsName) {
                    container = this._tabsList[this.options.miscTabsName];
                }
            } else {
                container = this._overlaysList
            }

        } else {
            container = this._baseLayersList;
        }

        controlgroup = L.DomUtil.create('div', 'control-group', container);
        checked = this._map.hasLayer(obj.layer);


        control = L.DomUtil.create('div', 'control', controlgroup);

        //label = document.createElement('label');
        //input = document.createElement('input');


        if (obj.overlay) {
            label = L.DomUtil.create('label', 'control-label', controlgroup);
            input = L.DomUtil.create('input', 'switch-small', control);

            input.type = 'checkbox';
            input.className = 'switch-control-layers-selector';
            input.defaultChecked = checked;
        } else {
            label = L.DomUtil.create('label', 'control-label-base', controlgroup);
            input = this._createRadioElement('leaflet-base-layers', checked);
        }

        input.layerId = L.stamp(obj.layer);
        input.control = this;

        if ((obj.name.length < 9 && !obj.overlayControl) || !obj.overlayControl) {
            if (obj.name.length > 19) {
                name = obj.name.substr(0, 19) + "...";
                label.innerHTML = '<abbr title="' + obj.name + '">' + name + '</abbr>';
            } else {
                label.innerHTML = obj.name;
            }
        } else {
            name = obj.name.substr(0, 9) + "...";
            label.innerHTML = '<abbr title="' + obj.name + '">' + name + '</abbr>';
        }

        control.appendChild(input);


        $(input).bootstrapSwitch({
            size: 'small'
        });
        $(input).on('switchChange.bootstrapSwitch', this._onInputClick);

        if (obj.overlay) {

            slider = this._createSliderElement(this, L.stamp(obj.layer));
            slider.layerId = L.stamp(obj.layer);
            controlgroup.appendChild(slider);
        }

        container.appendChild(controlgroup);

        return label;
    },

    _onInputClick: function (event, state) {
        event.preventDefault();
        event.stopPropagation();

        var layer = this.control._layers[this.layerId];

        this._handlingClick = true;


        if (layer.overlay) {

            if (state && !this.control._map.hasLayer(layer.layer)) {
                this.control._map.addLayer(layer.layer);
                $('#' + this.layerId).slider("option", "disabled", false);
            }
            else if (!state && this.control._map.hasLayer(layer.layer)) {
                this.control._map.removeLayer(layer.layer);
                $('#' + this.layerId).slider("option", "disabled", true);
            }
        }
        else {
            if (this.checked && !this.control._map.hasLayer(layer.layer)) {
                this.control._map.removeLayer(this.control._activeBaseLayer.layer);
                this.control._map.addLayer(layer.layer);
            }
        }

        var mapLayers = this.control._map._layers;

        for (var l in mapLayers) {
            var currentLayer = this.control._map._layers[l];
            if (currentLayer.hasOwnProperty("wmsParams")) {
                $('#' + currentLayer._leaflet_id).slider("option", "disabled", false);
                $('#' + currentLayer._leaflet_id).slider("option", "value", currentLayer.options.opacity);
            }
        }

        this._handlingClick = false;

    },

    _expand: function () {
        L.DomUtil.addClass(this._form, 'switch-control-layers-expanded');
    },

    _collapse: function () {
        if ($(this._form).hasClass('switch-control-layers-expanded')) {
            this._form.className = this._form.className.replace(' switch-control-layers-expanded', '');
        } else {
            L.DomUtil.addClass(this._form, 'switch-control-layers-expanded');
        }

    }
});

L.control.superiorlayermanager = function (baseLayers, overlays, tabs, options) {
    return new L.Control.SuperiorLayerManager(baseLayers, overlays, tabs, options);
};
