// Define the base layers
var openStreetMap = new L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    id: 'mapbox.streets'
});

var cartoDBLight = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
});

var cartoDBDark = L.tileLayer('http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
});

var _baseLayers = {
    'Open Street Map': openStreetMap,
    'CartoDB Light': cartoDBLight,
    'CartoDB Dark': cartoDBDark
};

var _overlaysWMS = {
    "geoserver01": {
        url: "http://siscom.ibama.gov.br/geoserver/csr",
        layers: {
            "Terras Indígenas": {
                layer: "csr:lim_terra_indigena_a",
                tab: "brazil"
            }
        }
    },

    "geoserver02": {
        url: "http://mhc-macris.net:8080/geoserver/MHC",
        layers: {
            "USA Massachusetts": {
                layer: "MHC:in_areas",
                tab: "vector"
            }
        }
    }

};

var _listOverlays = {};

